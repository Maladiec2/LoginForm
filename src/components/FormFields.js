import React, { Component } from 'react'
import axios from 'axios';

export class FormFields extends Component {

    constructor(props) {
        super(props);

        this.state = {
            fields: { email: '', password: '' },
            errors: {},
            users: [

            ]
        }
    }

    handleFormValidation() {
        let fields = this.state.fields;
        let errors = {};
        let formIsValid = true;

        if (!fields["email"]) {
            formIsValid = false;
            errors["email"] = "Cannot be empty";
        }

        if (typeof fields["email"] !== "undefined") {
            let lastAtPos = fields["email"].lastIndexOf('@');
            let lastDotPos = fields["email"].lastIndexOf('.');

            if (!(lastAtPos < lastDotPos && lastAtPos > 0 && fields["email"].indexOf('@@') === -1 && lastDotPos > 2 && (fields["email"].length - lastDotPos) > 2)) {
                formIsValid = false;
                errors["email"] = "Email is not valid";
            }
        }

        if (!fields["password"]) {
            formIsValid = false;
            errors["password"] = "Cannot be empty";
        }

        if (typeof fields["password"] !== "undefined") {
            if (fields["password"].length < 4) {
                formIsValid = false;
                errors["password"] = "At least 4 symbols";
            }
        }

        this.setState({ errors: errors });
        return formIsValid;
    }

    contactSubmit(e) {
        e.preventDefault();
        if (this.handleFormValidation()) {
            console.log("Form submitted");
        } else {
            console.log("Form has errors.");
        }
    }

    handleChange(field, e) {
        let fields = this.state.fields;
        fields[field] = e.target.value;
        this.setState({ fields });
    }

    componentDidMount() {
        axios.get('http://localhost:3004/users')
            .then(res => this.setState({ users: res.data }));
        window.addEventListener('load', this.handleLoad);
    }

    userValidation() {
        let users = this.state.users;
        let fields = this.state.fields;
        let user = { ...users };
        for (let i = 0; i !== users.length; i++) {
            if (user[i].email === fields.email) {
                if (user[i].password === fields.password) {
                    if (isChecked === true) {
                        localStorage.setItem('users', JSON.stringify(user[i]));
                    }
                    else {
                        localStorage.clear();
                    }
                    alert('Greetings');
                    break;
                }
            }
            else if (i === users.length - 1) {
                alert('Email or password is wrong');
                break;
            }
        }
    }

    handleLoad() {
        let local = localStorage.getItem('users');
        let localUser = JSON.parse(local);
        if (localUser != null) {
            alert('Greetings');
        }

    }

    checkboxChange() {
        isChecked = !isChecked;
    }

    render() {
        return (
            <div>
                <form onSubmit={this.contactSubmit.bind(this)} className='formStyle' style={formStyle}>
                    <span className='error' style={error}>{this.state.errors["email"]}</span>
                    <input onChange={this.handleChange.bind(this, "email")} value={this.state.fields["email"]} className='textField' style={textField} type='Email' placeholder="Email"></input>
                    <br />
                    <span className='error' style={error}>{this.state.errors["password"]}</span>
                    <input onChange={this.handleChange.bind(this, "password")} value={this.state.fields["password"]} className='textField' style={textField} type='Password' placeholder="Password"></input>
                    <br />
                    <input onClick={this.userValidation.bind(this)} className="submitBtn" style={submitBtn} type="submit" value="Sign In"></input>
                    <label > Remember me </label>
                    <input onChange={this.checkboxChange.bind(this)} type='checkbox' ></input>
                </form>
            </div>
        )
    }
}

let isChecked = false;

const formStyle = {
    padding: '20px',
    marginBottom: '10px',

}
const textField = {
    border: '#ccc 2px solid',
    borderRadius: '3px',
    padding: '20px 0px',
    textAlign: 'left',
    width: '100%',
    marginBottom: '20px'
}
const submitBtn = {
    padding: '10px 20px',
    border: '#ccc 2px solid',
    borderRadius: '5px',
    backgroundColor: '#84159E',
    color: 'white',
}

const error = {
    border: 'none',
    borderRadius: '3px',
    backgroundColor: 'orangered',
    color: '#fff',
}

export default FormFields