import React, { Component } from 'react';
import FormFields from './FormFields';

class LoginForm extends Component {
    render() {
        return (
            <div className="formStyle" style={formStyle}>
                <header className="formHeader" style={formHeader}>
                    <h3>Login Form</h3>
                </header>
                <FormFields>
                </FormFields>
            </div>
        );
    }
}
const formStyle = {
    borderRadius: '3px',
    border: '1px solid #ccc',
    backgroundColor: '#f4f4f4'
}

const formHeader = {
    backgroundColor: '#84159E',
    padding: '20px',
    color: 'white'
}
export default LoginForm;
